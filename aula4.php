<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">

<head>
	<title> Aula 4 Conexão com Data Base (continuação)</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 1.22" />
	 <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	</head>

<body>
	<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="#"> Marcos Correia</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active">
							<a href="#">Meu Site</a>
						</li>
						<li>
							<a href="#"> Notícia </a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gauchão<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Inter</a>
								</li>
								<li>
									<a href="#"> Grémio</a>
								</li>
								<li>
									<a href="#"> Cruzeiro</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#"> N.H.</a>
								</li>
								<li class="divider">
								</li>
								
							</ul>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div> <button type="submit" class="btn btn-default">Submit</button>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="#">Link</a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				
			</nav>
	
			<div class="page-header">
				<h1>
					Clubes Classificados para as Finais 
				</h1>
			</div>
	<!--		<p>
		
	<?php  
	error_reporting(E_ALL); 
	ini_set('display_errors','on');
	require_once('database.php');	
	$sql= 'select * from marcas';
	$consulta= $conexao->prepare($sql);
	$consulta->execute();
	$dados= $consulta->fetchALL(PDO::FETCH_ASSOC);
	
		//print_r($dados);
		//var_dump($dados);
		//foreach($dados as $value){
			//echo $value['Nome'].'<br />';
		//}
			

?>
	</p> -->
		<table class="table table-bordered" >
			<thead>
				<tr> 	<th> Nome</th>	<th> Origem</th>  <th> Fundação</th>	<th>Presidente</th>			</tr>
			</thead>
			<tbody>
					<?php
						foreach($dados as $value){
							echo '<tr><td>'.$value['Nome'].'</td> <td>'.$value['Origem'].'</td><td>'.$value['Fundacao'].'</td><td>'.$value['Presidente'].'</td></tr>';
				}
					
					?>
			
			
			</tbody>
		</table>
</body>

</html>
