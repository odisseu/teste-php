<?php
error_reporting(E_ALL);
		ini_set('display_errors','on');
if(!empty($_GET) ){
	$id = $_GET ['id'];
		
		require_once ('database.php');
		$sql ='select * from marcas where id=?';
		$consulta = $conexao->prepare($sql);
		$consulta->execute(array($id));
		$dados = $consulta->fetch(PDO::FETCH_ASSOC);	
}

if(!empty($_POST) ){
$id = $_POST['id'];
$nome = $_POST['nome'];
$origem = $_POST['origem'];
$fundacao = $_POST['fundacao'];
$presidente = $_POST['presidente'];
require_once ('database.php');
		$sql ='UPDATE marcas SET 
		Nome = ?,
		Origem = ?,
		Fundacao = ?,
		Presidente = ?
		WHERE id = ?';
		
		$alteracao = $conexao->prepare($sql);
		$ok = $alteracao->execute(
		array ($nome,$origem,$fundacao,$presidente,$id));
		
		if (($ok)){
				if ($ok){
				$msg= '<p class="bg-success" > Marcas Alteradas com sucesso.  </p>';
				}else{
					$msg= '<p class="bg-danger" > Marcas  não Alteradas!  </p>';
			}
			}
		header('location:listagem.php?mensagem='.$msg);//redireciona para local especificado
}	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">

<head>
	<title> Aula  - 15/04/2015 -  Alteração - CRUD -- Formulário altera em PHP - </title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 1.22" />
	
	<script type="javascript" src="js/jquery.min.js"></script>
	<script type="javascript" src="js/bootstrap.min.js"></script>
	<script type="javascript" src="js/scripts.js"></script>
	<link type="text/css" rel="stylesheet" href="bootstrap.min.css" />	
	<!--<link type="text/css" rel="stylesheet" href="estilo1.css" />  -->
</head>

<body>
	
	<div  id="geral">
		<div id="dir">
			<h1> </h1>
		</div>
	
<!--  ********************************************************************************************************** -->  

<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> 
					 <a class="navbar-brand" href="#">Meu Site</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li>
							<a href="aula4.php">LISTAGEM</a>
						</li>
						
						<li class="active">
							<a href="inseri.php">INSERI </a>
						</li>
						<li class="active">
							<a href="#">ALTERA </a>
						</li>
						<li class="active">
							<a href="#">DELETA </a>
						</li>
						<li class="dropdown ">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">One more separated link</a>
								</li>
							</ul>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div> <button type="submit" class="btn btn-default">Submit</button>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="#">Link</a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				
			</nav>
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a> <span class="divider">/</span>
				</li>
				<li>
					<a href="#">Library</a> <span class="divider">/</span>
				</li>
				<li class="active">
					Data
				</li>
			</ul>
			<?php
			if (isset($ok)){
				if ($ok){
				echo '<p class="bg-success" > Marcas Alteradas com sucesso.  </p>';
				}else{
					echo'<p class="bg-danger" > Marcas  não Alteradas!  </p>';
			}
			}
			?>
			<div class="page-header">
			<p> Altera MARCA</p>	
			</div>
			<form class="form-horizontal" action= "altera.php" method="post">
				<input type = "hidden" name="id" value="<?php echo $id; ?>"/>
				<fieldset>
					<legend>Altera dados da Marcas</legend>
			
					<div class="form-group">
						<label class="col-md-4 control-label" for="nome" >Nome: </label>
						<div class="col-md-4">
							<input type = "text" name="nome" class="form-control input-md" value="<?php echo $dados['Nome']; ?>"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="nome" >Origem: </label>
						<div class="col-md-4">
							<input type = "text" name="origem" class="form-control input-md" value="<?php echo $dados['Origem']; ?>"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="fundacao" >Fundação: </label>
						<div class="col-md-4">
							<input type = "text" name="fundacao" class="form-control input-md" value="<?php echo $dados['Fundacao']; ?>"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="presidente" >Presidente: </label>
						<div class="col-md-4">
							<input type = "text" name="presidente" class="form-control input-md" value="<?php echo $dados['Presidente']; ?>"/>
						</div>
					</div>
					<div class="form-group text-center">
						<div class="col-md-8">
							<button type=submit class="btn-primary">Gravar </button> 
						</div>
					</div>		
				</fieldset>
			</form> 
				










	
	<div class = "footer well">
		<p>&copy; 2015 - Marcos Correia after josias santos de azevedo</p>
		</div>
	</div>
	
	</div>
</body>

</html>

